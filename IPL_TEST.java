import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class IPL_TEST {
        @Test
    public void test_MatchesCSV_File()
    {
        ArrayList<Object> finalArray=new ArrayList<>();
        ArrayList<Object> arrayList=new ArrayList<Object>();
        try {
            Scanner scanner = new Scanner(new File("matches.csv"));
            while(scanner.hasNextLine())
            {
                arrayList.add(scanner.nextLine());

            }

        } catch (Exception e){
            System.out.println(e);
        }
        assertNotEquals(arrayList.size(),0);

    }
    @Test
    public void test_DeliveriesCSV_File()
    {
        ArrayList<Object> finalArray=new ArrayList<>();
        ArrayList<Object> arrayList=new ArrayList<Object>();
        try {
            Scanner scanner = new Scanner(new File("deliveries.csv"));
            while(scanner.hasNextLine())
            {
                arrayList.add(scanner.nextLine());

            }

        } catch (Exception e){
            System.out.println(e);
        }
        assertNotEquals(arrayList.size(),0);

    }
    @Test
    public void testMatchDataFunction()
    {
        ArrayList<Object> testArrayList = Main.matchDataFunction();
        assertNotNull(testArrayList);
        assertEquals(testArrayList.size(),637);

    }
    @Test
    public void testDeliveryDataFunction()
    {
        ArrayList<Object> testArrayList = Main.deliveryDataFunction();
        assertNotNull(testArrayList);
        assertEquals(testArrayList.size(),150461);
        assertTrue(testArrayList.size()==150461);

    }
    @Test
    public void test_MatchesWonPerTeam()
    {
        HashMap<String, Integer> testArrayList = Main.matchesWonPerTeam();
        assertNotNull(testArrayList);
        assertEquals(testArrayList.size(),15);
        assertTrue(testArrayList.size()==15);

    }
    @Test
    public void test_MatchesPerYear()
    {
        HashMap<String, Integer> testArrayList = Main.matchesPerYear();
        assertNotNull(testArrayList);
        assertEquals(testArrayList.size(),10);
        assertTrue(testArrayList.size()==10);

    }
    @Test
    public void test_VenueUsedIn2016()
    {
        HashMap<String, Integer> testArrayList = Main.venueUsedIn2016();
        assertNotNull(testArrayList);
        assertEquals(testArrayList.size(),11);
        assertTrue(testArrayList.size()==11);

    }
    @Test
    public void test_ExtraRunsIn2016()
    {
        HashMap<String, Integer> testArrayList = Main.extraRunsIn2016();
        assertNotNull(testArrayList);
        assertEquals(testArrayList.size(),8);
        assertTrue(testArrayList.size()==8);

    }
    @Test
    public void test_TopEconomy()
    {
        List<List> testArrayList = Main.topEconomy();
        assertNotNull(testArrayList);
        assertEquals(testArrayList.get(0).size(),3);
        assertTrue(testArrayList.get(0).size()==3);
        assertEquals(testArrayList.get(0).get(0).toString().split("=")[0],"HV Patel");
        assertEquals(testArrayList.get(0).get(1).toString().split("=")[0],"AF Milne");
        assertEquals(testArrayList.get(0).get(2).toString().split("=")[0],"Harbhajan Singh");

    }


}
