import java.io.File;
import java.util.*;

import static java.lang.Integer.parseInt;

public class Main {
    public static void displayResult(HashMap<String , Integer>hashMap){
        for(Map.Entry<String,Integer> set:hashMap.entrySet()){
            System.out.println(set.getKey()+"="+set.getValue());
        }
    }
    public static ArrayList<Object> matchDataFunction(){
        HashMap<String, Integer> resultObj = new HashMap<>();
        ArrayList<Object> finalArray=new ArrayList<>();
        ArrayList<Object> arrayList=new ArrayList<Object>();
        try {
            Scanner scanner = new Scanner(new File("matches.csv"));
            while(scanner.hasNextLine())
            {
                arrayList.add(scanner.nextLine());

            }

        } catch (Exception e){
            System.out.println(e);
        }
        return arrayList;
    }
    public static ArrayList<Object> deliveryDataFunction(){
        HashMap<String, Integer> resultObj = new HashMap<>();
        ArrayList<Object> finalArray=new ArrayList<>();
        ArrayList<Object> arrayList=new ArrayList<Object>();
        try {
            Scanner scanner = new Scanner(new File("deliveries.csv"));
            while(scanner.hasNextLine())
            {
                arrayList.add(scanner.nextLine());

            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return arrayList;
    }
    public static HashMap<String, Integer> matchesPerYear(){
        ArrayList<Object> matchArrayList = matchDataFunction();
        ArrayList<Object> keyValues = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        HashMap<String ,Integer> resultObj = new HashMap<>();
        keyValues.add(matchArrayList.get(0));

        String keyStr = keyValues.toString();
        String[] keyArray = keyStr.split(",");
        for (int listIndex=1; listIndex<matchArrayList.size();listIndex++){
            String tempValue = matchArrayList.get(listIndex).toString();
            String[] tempArr = tempValue.split(",");
            for (int tempArrIndex=0; tempArrIndex<tempArr.length; tempArrIndex++){
                hashMap.put(keyArray[tempArrIndex], tempArr[tempArrIndex]);
            }

            if(resultObj.containsKey(hashMap.get("season"))){
                resultObj.put(hashMap.get("season"),resultObj.get(hashMap.get("season"))+1);
            } else {
                resultObj.putIfAbsent(hashMap.get("season"),1);
            }

        }
        System.out.println("Number of matches played per year");
        displayResult(resultObj);
        return resultObj;


    }
    public static HashMap<String ,Integer> matchesWonPerTeam(){
        ArrayList<Object> matchArrayList = matchDataFunction();
        ArrayList<Object> keyValues = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        HashMap<String ,Integer> resultObj = new HashMap<>();
        keyValues.add(matchArrayList.get(0));

        String keyStr = keyValues.toString();
        String[] keyArray = keyStr.split(",");
        for (int listIndex=1; listIndex<matchArrayList.size();listIndex++){
            String tempValue = matchArrayList.get(listIndex).toString();
            String[] tempArr = tempValue.split(",");
            for (int tempArrIndex=0; tempArrIndex<tempArr.length; tempArrIndex++){
                hashMap.put(keyArray[tempArrIndex], tempArr[tempArrIndex]);
            }

            if(resultObj.containsKey(hashMap.get("winner"))){
                resultObj.put(hashMap.get("winner"),resultObj.get(hashMap.get("winner"))+1);
            } else {
                resultObj.putIfAbsent(hashMap.get("winner"),1);
            }
        }
        System.out.println("Matches Won per Team");
        displayResult(resultObj);

        return resultObj;

    }
    public static HashMap<String ,Integer>  venueUsedIn2016(){
        ArrayList<Object> matchArrayList = matchDataFunction();
        ArrayList<Object> keyValues = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        HashMap<String ,Integer> resultObj = new HashMap<>();
        keyValues.add(matchArrayList.get(0));

        String keyStr = keyValues.toString();
        String[] keyArray = keyStr.split(",");
        for (int listIndex=1; listIndex<matchArrayList.size();listIndex++) {
            String tempValue = matchArrayList.get(listIndex).toString();
            String[] tempArr = tempValue.split(",");
            for (int tempArrIndex = 0; tempArrIndex < tempArr.length; tempArrIndex++) {
                hashMap.put(keyArray[tempArrIndex], tempArr[tempArrIndex]);
            }

            if (hashMap.get("season").equals("2016")) {

                if(resultObj.containsKey(hashMap.get("venue"))){
                    resultObj.put(hashMap.get("venue"),resultObj.get(hashMap.get("venue"))+1);
                } else {
                    resultObj.putIfAbsent(hashMap.get("venue"),1);
                }
            }

        }
        System.out.println("Following venues were used these number of times");
        displayResult(resultObj);

        return resultObj;

    }
    public static HashMap<String, Integer> extraRunsIn2016(){
        ArrayList<Object> matchArrayList = matchDataFunction();
        ArrayList<Object> deliveryArrayList = deliveryDataFunction();
        ArrayList<Object> keyValues = new ArrayList<>();
        HashMap<String, String> hashMap1 = new HashMap<>();
        HashMap<String ,HashMap> resultObj = new HashMap<>();
        HashMap<String,HashMap> finalObj = new HashMap<>();
        HashMap<String,Integer> extraRunsObject = new HashMap<>();
        keyValues.add(deliveryArrayList.get(0));


        String keyStr = keyValues.toString();

        String[] keyArray = keyStr.split(",");

        for (int listIndex=1; listIndex<deliveryArrayList.size();listIndex++){
            String tempValue = deliveryArrayList.get(listIndex).toString();
            String[] tempArr = tempValue.split(",");

            for (int tempArrIndex=0; tempArrIndex<tempArr.length; tempArrIndex++){
                hashMap1.put(keyArray[tempArrIndex], tempArr[tempArrIndex]);
            }
            if(resultObj.containsKey(hashMap1.get("[    match_id"))){
                if(resultObj.get(hashMap1.get("[    match_id")).containsKey(hashMap1.get("bowling_team"))){
                    resultObj.get(hashMap1.get("[    match_id")).put(hashMap1.get("bowling_team"),parseInt(resultObj.get(hashMap1.get("[    match_id")).get(hashMap1.get("bowling_team")).toString())+parseInt(hashMap1.get("extra_runs")));
                } else {
                    resultObj.get(hashMap1.get("[    match_id")).put(hashMap1.get("bowling_team"),parseInt(hashMap1.get("extra_runs")));
                }

            } else {
                resultObj.putIfAbsent(hashMap1.get("[    match_id"),new HashMap<String,Integer>());


            }


        }

        for (int matchListIndex=1; matchListIndex<matchArrayList.size();matchListIndex++){
            String tempValue = matchArrayList.get(matchListIndex).toString();


            String[] tempArr = tempValue.split(",");
            if(parseInt(tempArr[1])==2016){
                if(resultObj.containsKey(tempArr[0])){
                    finalObj.put(tempArr[0],resultObj.get(tempArr[0]));
                }
            }
        }

        for (HashMap.Entry<String, HashMap> setOne : finalObj.entrySet()) {


            for (Object setTwo : setOne.getValue().entrySet()){
                HashMap<String, Integer> tempHashMap = new HashMap<>();
                setTwo.toString().split("=");
                tempHashMap.put( setTwo.toString().split("=")[0], parseInt( setTwo.toString().split("=")[1]));
                if(extraRunsObject.containsKey(setTwo.toString().split("=")[0])){
                    extraRunsObject.put(setTwo.toString().split("=")[0],extraRunsObject.get(setTwo.toString().split("=")[0])+parseInt( setTwo.toString().split("=")[1]));
                } else {
                    extraRunsObject.putIfAbsent(setTwo.toString().split("=")[0],parseInt( setTwo.toString().split("=")[1]));
                }

            }

        }

        System.out.println("Extra Runs Conceded per Team in 2016 are");
        displayResult(extraRunsObject);

        return extraRunsObject;
    }








    public static List<List> topEconomy() {
        ArrayList<Object> matchArrayList = matchDataFunction();
        ArrayList<Object> deliveryArrayList = deliveryDataFunction();
        ArrayList<Object> keyValues = new ArrayList<>();
        ArrayList<String> finalArray = new ArrayList<>();
        HashMap<String, HashMap> bowlersObject = new HashMap<>();
        HashMap<String, Integer> economyObject = new HashMap<>();
        ArrayList<HashMap.Entry> economyArray = new ArrayList<>();
        List<List> topEconomyList = new ArrayList<>();
        keyValues.add(deliveryArrayList.get(0));


        for (int matchListIndex = 1; matchListIndex < matchArrayList.size(); matchListIndex++) {
            String tempValue = matchArrayList.get(matchListIndex).toString();


            String[] tempArr = tempValue.split(",");
            if (parseInt(tempArr[1]) == 2016) {

                finalArray.add(tempArr[0]);

            }

        }

        for (int deliveryArrayIndex = 0; deliveryArrayIndex < deliveryArrayList.size(); deliveryArrayIndex++) {
            String wide_runs = (deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[11]);
            String leg_bye = (deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[13]);
            String bye_runs = (deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[12]);
            String no_ball = (deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[14]);
            String batsman_runs = (deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[16]);
            String extra_runs = (deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[17]);
            if (finalArray.contains(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[0])) {

                if (bowlersObject.containsKey(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8])) {
                    if (parseInt(wide_runs.toString()) == 0 && parseInt(no_ball.toString()) == 0) {
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("balls", parseInt(bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).get("balls").toString()) + 1);
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("runs", parseInt(bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).get("runs").toString()) + parseInt(batsman_runs.toString()) + parseInt(extra_runs.toString()));
                    }
                    if (parseInt(wide_runs.toString()) == 1 && parseInt(no_ball.toString()) == 0) {

                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("runs", Integer.parseInt(bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).get("runs").toString()) + parseInt(batsman_runs.toString()) + parseInt(extra_runs.toString()));
                    }
                    if (parseInt(wide_runs.toString()) == 0 && parseInt(no_ball.toString()) == 1) {

                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("runs", Integer.parseInt(bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).get("runs").toString()) + batsman_runs + extra_runs);
                    }

                } else {
                    bowlersObject.putIfAbsent(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8], new HashMap<>());

                    if (parseInt(wide_runs.toString()) == 0 && parseInt(leg_bye.toString()) == 0 && parseInt(bye_runs.toString()) == 0 && parseInt(no_ball.toString()) == 0) {
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("balls", 1);
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("runs", parseInt(batsman_runs.toString()) + parseInt(extra_runs.toString()));
                    }
                    if (parseInt(wide_runs.toString()) == 0 && parseInt(extra_runs.toString()) != 0 && parseInt(no_ball.toString()) == 0) {
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("balls", 1);
                        bowlersObject.get(deliveryArrayList.get(deliveryArrayIndex).toString().split(",")[8]).put("runs", 0);
                    }
//


                }

            }
        }

        for (HashMap.Entry<String, HashMap> setOne : bowlersObject.entrySet()) {
            int balls = (int) setOne.getValue().get("balls");
            int runs = (int) setOne.getValue().get("runs");
            economyObject.put(setOne.getKey(), (runs / balls) * 6);
        }
        for (HashMap.Entry<String, Integer> setOne : economyObject.entrySet()) {
            economyArray.add(setOne);
        }
        topEconomyList.add(economyArray.subList(0,3));
        System.out.println("Top 3 Economical Bowlers");
        return topEconomyList;


    }



public static void topThreePlayersOf2016() {
        HashMap<String, HashMap> resultObj = new HashMap<>();
        resultObj.put("batsman", new HashMap<>());
        resultObj.put("bowler", new HashMap<>());
        ArrayList<String> batsmanArray = new ArrayList<>();
        ArrayList<Object> matchDataArray = matchDataFunction();
        ArrayList<Object> deliveryDataArray = deliveryDataFunction();
        HashMap<String, String> idObject = new HashMap<>();
        for (int idArrayIndex = 1; idArrayIndex < matchDataArray.size(); idArrayIndex++) {
            String id = matchDataArray.get(idArrayIndex).toString().split(",")[0];
            String season = matchDataArray.get(idArrayIndex).toString().split(",")[1];
//
            if (parseInt(season) == 2016) {


                idObject.put(id, season);
            }

        }


        for (int deliveryArrayIndex = 1; deliveryArrayIndex < deliveryDataArray.size(); deliveryArrayIndex++) {
            if (idObject.containsKey(deliveryDataArray.get(deliveryArrayIndex).toString().split(",")[0])) {
                String batsmen = deliveryDataArray.get(deliveryArrayIndex).toString().split(",")[6];
                String runs = deliveryDataArray.get(deliveryArrayIndex).toString().split(",")[15];
                String bowler = deliveryDataArray.get(deliveryArrayIndex).toString().split(",")[8];
                if(deliveryDataArray.get(deliveryArrayIndex).toString().split(",").length<=18) {
                String runsGiven = deliveryDataArray.get(deliveryArrayIndex).toString().split(",")[17];
                }
                   if(resultObj.get("batsman").containsKey(batsmen)){
                     resultObj.get("batsman").put(batsmen, parseInt(resultObj.get("batsman").get(batsmen).toString())+parseInt(runs));
//
                   } else {
                       resultObj.get("batsman").putIfAbsent(batsmen, parseInt(runs));
                   }
                if(resultObj.get("bowler").containsKey(bowler)){

                        resultObj.get("bowler").put(bowler, parseInt(resultObj.get("bowler").get(bowler).toString()) + 1);

//
                } else {

                        resultObj.get("bowler").putIfAbsent(bowler, 1);

                }
                }

            }
             System.out.println(resultObj);
        }

    public static void main(String[] args){
//     matchesPerYear();
//     matchesWonPerTeam();
//     venueUsedIn2016();
//        extraRunsIn2016();
        // topThreePlayersOf2016();
//topEconomy();

    }
}
